var Cl = window.Cl || {};

(function ($) {
    'use strict';

    Cl.Templator = new Class({
        option: {},

        data: {},

        events: {
            onInit: function () {},
            onDataUpdate: function () {},
            beforeRender: function () {},
            afterRender: function () {}
        },

        initialize: function (options) {
            this.options = $.extend(true, {}, this.options, options);
            this.events = $.extend(true, {}, this.events, this.options.events);
            this._initUI();
            if (this._ui.container.length) {
                $.extend(this.data, this._ui.container.data());
                this.update();
            }
            this.events.onInit.call(this);
        },

        _initUI: function () {
            this._ui = {};
            this._ui.container = $(this.options.container);
            this._ui.templateContainer = $(this._ui.container.data('template'));
            this.templateHtml = this._ui.templateContainer.html();
        },

        _render: function () {
            this.events.onDataUpdate.call(this, this.data);
            this.events.beforeRender.call(this, this.data);
            this._ui.container.html(_.template(this.templateHtml, this.data));
            this.events.afterRender.call(this, this.data);
        },

        update: function () {
            var that = this;
            if (this.data.json) {
                $.extend(this.data, this.data.json);
                this._render();
            } else if (this.data.jsonAjax) {
                $.get(this.data.jsonAjax, function(data) {
                    $.extend(that.data, data);
                    that._render();
                });
            } else {
                that._render();
            }
        }
    });

})(jQuery);
