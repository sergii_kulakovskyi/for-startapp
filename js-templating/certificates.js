$(function () {
    'use strict';

    function gridsData(data) {
        var scaleRange = data.xTo - data.xFrom;
        var gridColsCount = scaleRange / data.gridStep;
        data.grid = [];
        for (var i = 0; i <= gridColsCount; i++) {
            data.grid.push({
                number: i * data.gridStep + data.xFrom,
                position: i / gridColsCount * 100 + '%'
            });
        }
    }

    $('.js-vocational-report').each(function () {
        var vocationReport = $(this);
        vocationReport.data('templator', new Cl.Templator({
            container: $(this),
            events: {
                onDataUpdate: function (data) {
                    data.forcedVocation = vocationReport.attr('data-forced-vocation');
                    var scaleRange = data.xTo - data.xFrom;
                    var toPercent = function (number) {
                        return number / scaleRange * 100 + '%';
                    };
                    gridsData(data);
                    data.scales = {};
                    if (data.forcedVocation) {
                        _.each(data.studentScalesData, function (item) {
                            item.vocationScore = data.vocations[data.forcedVocation][item.id];
                            item.positionLeft = toPercent(item.lower_limit - data.xFrom);
                            item.positionCenterLeft = toPercent(item.score - data.xFrom);
                            item.positionCenterRight = toPercent(data.xTo - item.score);
                            item.positionRight = toPercent(data.xTo - item.upper_limit);
                            item.vocationPositionLeft = toPercent(item.vocationScore - data.xFrom);
                            item.vocationPositionRight = toPercent(data.xTo - item.vocationScore);
                        });
                    }
                }
            }
        }));
    });

    $('.js-certificates-chart-graph-bar').each(function () {
        var bar = $(this);
        var data = bar.data();
        bar.css({
            left: (data.lowerLimit - data.xFrom) / (data.xTo - data.xFrom) * 100 + '%',
            right: (data.xTo - data.upperLimit) / (data.xTo - data.xFrom) * 100 + '%'
        });
    });

    $('.js-certificates-chart-grid').each(function () {
        new Cl.Templator({
            container: $(this),
            events: {
                onDataUpdate: gridsData
            }
        });
    });

    $('.js-level-score').each(function () {
        var level = $(this);
        var data = level.data();
        level.text(data.competenceLevelLabels[data.level - 1]);
    });

    $('.js-certificates-chart-graph-levels').each(function () {
        new Cl.Templator({
            container: $(this)
        });
    });
});
